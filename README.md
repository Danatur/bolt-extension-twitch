Twitch
======================

This [bolt.cm](https://bolt.cm/) extension will get raw data from Twitch's public API. You can display channel and stream informations with handy widgets on you Bolt website.

### Installation
1. Login to your Bolt installation
2. Go to "Extend" or "Extras > Extend"
3. Type "twitch" into the input field
4. Click on the extension name
5. Click on "Browse Versions"
6. Click on "Install This Version" on the latest stable version

------

### Requirements
- PHP 7+
- Bolt 3+

------

### Configuration
Go to the config ``twitch.danatur.yml``. You have to set the following:

**1. Your Client-ID**
```
    clientId: "h8ythpmmn8hfaw4n4863kwbk7vq6yj"
```

To generate a client ID follow the instructions below:

1. Go to the Twitch Dev site (https://dev.twitch.tv/) and log in to your account.
2. In your dashboard got to tab "Apps", choose "Register an App".
3. On the "Register Your Application" page, complete the form and choose Register.
4. You get a generated Clien-ID, copy it to the ``twitch.danatur.yml`` of the installed Twitch extension.


**2. Choose Twitch channel/user you want to show**

```
users:
    - twitchchannel 1
    - twitchchannel 2
    - ...
```

You can set one or more channels.


**3. Set cache**
```

timeouts:
    user: 300
    channel: 30
    game: 86400
```

Time of the cache where the informations from Twich are stored. By default set to 300, 30, 86400.


### Example and usage
Use the "raw" filter in order to display the template properly. 

```
{{ twitchStatus()|raw }}
```
Displays the Twitch infromations of all channels in the config.yml.


```
{{ twitchStatus([Channelname])|raw }}
```
Only displays the Twitch infromations of the specified channel name. Note: the spcified channels need to be in the ``twitch.danatur.yml``.


```
{{ twitchStatus([channelname1, channelname2, channelname3])|raw }}
```
You also can pick just a few names from the list.


#### There are three display types

```
{{ twitchStatus()|raw }}
```

Shows only a simple online/offline status of a channel.

```
{{ twitchWidget()|raw }}
```

Shows a big widget with a screenshot and stream/channel imformations.


```
{{ twitchWidgetList()|raw }}
```

Shows a list with small widgets with a screenshot and stream/channel imformations.


### Extra hint

You can ad a textfield in you content type to set the channelname you want to show in the backend of the desired site. It's more flexible. To show it in the template use:

```
{{ twitchWidget([record.streamfield|trim])|raw }}
```

Note: ``record.streamfield`` is the variable of the field in the contenttype.


------

### Notes
Shout out to Parrakh! ❤️
