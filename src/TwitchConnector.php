<?php

namespace Danatur\Twitch;


/**
 * Twitch connector class.
 *
 * @author C. Kappes <ckappes@venlegis.de>
 */
class TwitchConnector
{

    protected $client = null;

    protected $cache = null;

    protected $config = null;


    public function __construct($client, $cache, $config)
    {
        $this->client = $client;
        $this->cache = $cache;
        $this->config = $config;
    }


    public function getData($users)
    {
        $jsonData = $this->getCombinedData($users);

        return $jsonData;
    }

    public function getWidget()
    {
        $jsonData = $this->loadUsersInfo($this->config['users']);
    }

    protected function getCombinedData($users)
    {      
        $usersCount = count($users);
        $swappedUsers = array_flip($users);
         

        $streamData = $this->loadStreamsInfo($this->config['users']);
        
        $gameIds = [];
        $formatStream = [];
        if (isset($streamData['data'])) {
            foreach ($streamData['data']  as $stream) {
                $formatStream[$stream['user_id']] = $stream;
                $gameIds[] = $stream['game_id'];
            }
        }

        $gameData = $this->loadGamesInfo($gameIds);

        $formatGame = [];   
        if (isset($gameData['data'])) { 
            foreach ($gameData['data'] as $game) {
                $formatGame[$game['id']] = $game;
            }
        }

        foreach ($formatStream as $streamId => $stream) {
            $game = null;

            if (isset($formatGame[$stream['game_id']])) {
                $game = $formatGame[$stream['game_id']];
            }

            $formatStream[$streamId]['currentGame'] = $game;
        }

        $userData = $this->loadUsersInfo($this->config['users']);

        $combinedData = [];
        if (isset($userData['data'])) {
            foreach ($userData['data'] as $user) {
                if ($usersCount === 0 || isset($swappedUsers[$user['login']])) {
                    $stream = null;
                    if (isset($formatStream[$user['id']])) {
                        $stream = $formatStream[$user['id']];
                    }

                    $user['currentStream'] = $stream;

                    $combinedData[] = $user;
                }
            }
        }

        return $combinedData;
    }


    protected function loadUsersInfo($users)
    {
        $cacheKey = "twitch|users|" . implode($users, ":");


        $jsonData = $this->cache->fetch($cacheKey);

        if ($jsonData === false) {
            $jsonData = null;

            try {
                $url = "https://api.twitch.tv/helix/users?";

                foreach ($users as $user) {
                    $url .= "login=" . $user . "&";
                }

                $url .= "first=100";

                $response =  $this->client->request(
                    "GET",
                    $url,
                    [
                        "headers" => [
                            "Client-ID" => $this->config['clientId'],
                        ]
                    ]
                );

                if ($response->getStatusCode() === 200) {
                    $jsonData = $response->getBody()->getContents();
                }
            } catch (\Exception $e) {

            }

            if ($jsonData !== null) {
                $this->cache->save($cacheKey, $jsonData, $this->config['timeouts']['user']);         
            }
        }

        if ($jsonData !== null) {
            $jsonData = json_decode($jsonData, true);
        }

        

        return $jsonData;
    }


    protected function loadStreamsInfo($users)
    {
        $cacheKey = "twitch|streams|" . implode($users, ":");


        $jsonData = $this->cache->fetch($cacheKey);

        if ($jsonData === false) {
            $jsonData = null;

            try {
                $url = "https://api.twitch.tv/helix/streams?";

                foreach ($users as $user) {
                    $url .= "user_login=" . $user . "&";
                }

                $response =  $this->client->request(
                    "GET",
                    $url,
                    [
                        "headers" => [
                            "Client-ID" => $this->config['clientId'],
                        ]
                    ]
                );

                if ($response->getStatusCode() === 200) {
                    $jsonData = $response->getBody()->getContents();
                }
            } catch (\Exception $e) {

            }

            if ($jsonData !== null) {
                $this->cache->save($cacheKey, $jsonData, $this->config['timeouts']['channel']);         
            }
        }

        if ($jsonData !== null) {
            $jsonData = json_decode($jsonData, true);
        }

        

        return $jsonData;
    }


    protected function loadGamesInfo($games)
    {
        $cacheKey = "twitch|games|" . implode($games, ":");


        $jsonData = $this->cache->fetch($cacheKey);

        if ($jsonData === false) {
            $jsonData = null;

            try {
                $url = "https://api.twitch.tv/helix/games?";

                foreach ($games as $game) {
                    $url .= "id=" . $game . "&";
                }

                $response =  $this->client->request(
                    "GET",
                    $url,
                    [
                        "headers" => [
                            "Client-ID" => $this->config['clientId'],
                        ]
                    ]
                );

                if ($response->getStatusCode() === 200) {
                    $jsonData = $response->getBody()->getContents();
                }
            } catch (\Exception $e) {

            }

            if ($jsonData !== null) {
                $this->cache->save($cacheKey, $jsonData, $this->config['timeouts']['game']);         
            }
        }

        if ($jsonData !== null) {
            $jsonData = json_decode($jsonData, true);
        }


        return $jsonData;
    }

}