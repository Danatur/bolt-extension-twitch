<?php

namespace Danatur\Twitch;


use Bolt\Asset\File\Stylesheet;
use Bolt\Controller\Zone;
use Bolt\Extension\SimpleExtension;
use Silex\Application;
use Twig\Environment;
use Bolt\Translation\Translator as Trans;

/**
 * Twitch extension class.
 *
 * @author C. Kappes <ckappes@venlegis.de>
 */
class TwitchExtension extends SimpleExtension
{

    protected function registerAssets() {

        return [
            (new Stylesheet('twitch.css'))
                ->setPriority(0)
        ];
    }

    

    protected $connector = null;

    /**
     * {@inheritdoc}
     */
    public function boot(Application $app)
    {
        parent::boot($app);

        $this->connector = new TwitchConnector(
            $this->container['guzzle.client'],
            $this->container['cache'],
            $this->getConfig()
        );

        // Twig extension for time_diff modifier
        $this->container['twig'] = $app->share(
            $app->extend(
                'twig',
                function (Environment $twig, $app) {
                    $twig->addExtension(new \Twig_Extensions_Extension_Date());

                    return $twig;
                }
            )
        );
    }


    /**
     * {@inheritdoc}
     */
    protected function registerTwigFunctions()
    {
        return [
            'twitchStatus' => 'twitchStatus',
            'twitchWidget' => 'twitchWidget',
            'twitchWidgetList' => 'twitchWidgetList',
        ];
    }

    /**
     * The callback function when {{ my_twig_function() }} is used in a template.
     *
     * @return string
     */
    public function twitchStatus($users = [])
    {
        return $this->renderTemplate(
            'status.twig', 
            [
                "entries" => $this->connector->getData($users),
            ]
        );
    }

    public function twitchWidget($users = [])
    {
        return $this->renderTemplate(
            'widget.twig', 
            [
                "entries" => $this->connector->getData($users),
            ]
        );
    }

    public function twitchWidgetList($users = [])
    {
        return $this->renderTemplate(
            'widget_list.twig', 
            [
                "entries" => $this->connector->getData($users),
            ]
        );
    }

}
